ruleset temperature_store {
  meta {
    shares __testing,
        temperatures, threshold_violations, inrange_temperatures
    provides temperatures, threshold_violations, inrange_temperatures
  }
  global {
    __testing = {"queries":
      [{"name": "__testing" }], 
      "events": [
        {"domain": "wovyn", "type": "new_temperature_reading", 
            "attrs": ["temperature", "timestamp"]},
        {"domain": "wovyn", "type": "threshold_violation",
            "attrs": ["temperature", "timestamp"]},
        {"domain": "sensor", "type": "reading_reset"}
      ]
    }
    
    temperatures = function() {
      ent:temperatures
    }
    threshold_violations = function() {
      ent:violations
    }
    inrange_temperatures = function() {
      ent:temperatures.filter(function(temperature) {
        not (ent:violations >< temperature)
      })
    }
  }
  
  rule collect_temperatures {
    select when wovyn new_temperature_reading
    pre {
      b = event:attr("temperature").klog()
    }
    always {
      ent:temperatures := ent:temperatures.defaultsTo([]).append({
        "temperature": event:attr("temperature")[0]{"temperatureF"},
        "timestamp": event:attr("timestamp")
      })
      // ent:temperature := event:attr("temperature");
      // ent:timestamp := event:attr("timestamp")
    }
  }
  
  rule collect_threshold_violations {
    select when wovyn threshold_violation
    always {
      ent:violations := ent:violations.defaultsTo([]).append({
        "temperature": event:attr("temperature"),
        "timestamp": event:attr("timestamp")
      })
    }
  }
  
  rule clear_temperatures {
    select when sensor reading_reset
    always {
      ent:temperatures := [];
      ent:violations := []
    }
  }
  
}
